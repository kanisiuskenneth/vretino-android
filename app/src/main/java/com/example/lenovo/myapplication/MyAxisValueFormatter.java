package com.example.lenovo.myapplication;

/**
 * Created by lenovo on 5/14/2018.
 */

interface MyAxisValueFormatter {
    int getDecimalDigits();
}