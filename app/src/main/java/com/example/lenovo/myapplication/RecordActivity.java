package com.example.lenovo.myapplication;

/**
 * Created by lenovo on 4/6/2018.
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;


public class RecordActivity extends AppCompatActivity{


    ArrayList<String> dataEntry = new ArrayList<>();
    List<String[]> dataEntry2 = new ArrayList<>();
    TextView txtString, txtStringLength, txtstatuseeg;
    Handler bluetoothIn;
    ImageButton btnStartRec, btnConvertCSV, btnReceiveData, btnResGraph;
    Button  btnCekArray;

    final int handlerState = 0;        				 //used to identify handler message
    private BluetoothAdapter btAdapter = null;
    private BluetoothSocket btSocket = null;
    private StringBuilder recDataString = new StringBuilder();

    private ConnectedThread mConnectedThread;
    private boolean transferring = false;
    OutputStreamWriter outputStreamWriter = null;
    FileOutputStream fileOutput = null;

    // SPP UUID service - this should work for most devices
    private static final UUID BTMODULEUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    // String for MAC address
    private static String address = "00:21:13:02:1F:1E";
    int k = 1;

    public RecordActivity() throws FileNotFoundException {
    }

    private void onFinishedTransferData(String message) {
        String[] rows = message.split("~");
        for(String row: rows) {
            if(row.equals("---EOF---")) {
                break;
            }
            if(row.charAt(0) == '#') {
                dataEntry.add(row.substring(1));
            }
            else if(row.charAt(0) == '*')
            {
                String sensor1 = row.substring(1, 18);
                txtstatuseeg.setText(sensor1);
            }

            else if(row.charAt(0) == '^')
            {
                String sensor2 = row.substring(1, 4);
                txtstatuseeg.setText(sensor2);
            }
            recDataString.delete(0, recDataString.length());
        }
        System.out.println(dataEntry.size());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bt_activity);

        //Link the buttons and textViews to respective views
        btnStartRec = (ImageButton) findViewById(R.id.startRec);
        btnConvertCSV = (ImageButton) findViewById(R.id.convCSV);
        btnReceiveData = (ImageButton) findViewById(R.id.recData);
        btnResGraph = (ImageButton) findViewById(R.id.resGraph);
        btnCekArray = (Button) findViewById(R.id.button6);
        txtString = (TextView) findViewById(R.id.txtStringbawah);
        txtStringLength = (TextView) findViewById(R.id.testView1);
        txtstatuseeg = (TextView) findViewById(R.id.statuseeg);

        //writeCSV
        final File file = new File(getBaseContext().getExternalFilesDir(null), "apalahartiakumenunggu1.csv");

        /**try {
            outputStreamWriter = new OutputStreamWriter(fileOutput = new FileOutputStream(file));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }**/

        address = getIntent().getStringExtra(DeviceListActivity.EXTRA_DEVICE_ADDRESS);


        //Handler
        bluetoothIn = new Handler() {
            public void handleMessage(android.os.Message msg) {
                if (msg.what == handlerState) {                                        //if message is what we want
                    String readMessage = (String) msg.obj;
                    recDataString.append(readMessage);
                    if (!transferring) {

                    }
                    else {
                        // msg.arg1 = bytes from connect thread
                        recDataString.append(readMessage);
                        Log.d("RecordActivity", "Stringbuff: "+ recDataString.length());
                        if (recDataString.indexOf("---EOF---") != -1){
                            transferring = false;
                            //Done transferring string, call Callback function
                            Log.d("RecordActivity", "Done transfer");
                            onFinishedTransferData(recDataString.toString());
                        }//keep appending to string until ~
                    }
                }



            }
        };

        btAdapter = BluetoothAdapter.getDefaultAdapter();       // get Bluetooth adapter
        checkBTState();


        // Set up onClick listeners for buttons to send 1 or 0 to turn on/off LED
        btnStartRec.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                mConnectedThread.write("r");    // Send "0" via Bluetooth
                //System.out.println("halo: " + dataEntry);
            }
        });

        btnConvertCSV.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                mConnectedThread.write("c");    // Send "1" via Bluetooth
                Toast.makeText(getBaseContext(), "Tunggu 3 Detik sebelum tekan tombol nomor 3", Toast.LENGTH_SHORT).show();
            }
        });

        btnCekArray.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                System.out.println("Halo: " + dataEntry.size());
                //write array to csv
                try {
                    FileWriter writer = new FileWriter(file);
                    for (String str: dataEntry ){
                        writer.write(str);
                        writer.write("\n");
                    }
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Toast.makeText(getBaseContext(), "Data Saved", Toast.LENGTH_SHORT).show();

            }
        });



        btnResGraph.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                /**try {
                    FileWriter writer = new FileWriter(file);
                    for (String str: dataEntry ){
                        writer.write(str);
                        writer.write("\n");
                    }
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }**/
                Intent intent = new Intent(RecordActivity.this, ResultActivity.class);
                startActivity(intent);
            }
        });

        btnReceiveData.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                transferring = true;
                mConnectedThread.write("s");    // Send "0" via Bluetooth
                Toast.makeText(getBaseContext(), "Turn off LED", Toast.LENGTH_SHORT).show();
            }
    });
    }

    private BluetoothSocket createBluetoothSocket(BluetoothDevice device) throws IOException {

        return  device.createRfcommSocketToServiceRecord(BTMODULEUUID);
        //creates secure outgoing connecetion with BT device using UUID
    }

    @Override
    public void onResume() {
        super.onResume();

        //Get MAC address from DeviceListActivity via intent
        //Intent intent = getIntent();

        //Get the MAC address from the DeviceListActivty via EXTRA
        //address = intent.getStringExtra(DeviceListActivity.EXTRA_DEVICE_ADDRESS);

        //create device and set the MAC address
        BluetoothDevice device = btAdapter.getRemoteDevice(address);

        try {
            btSocket = createBluetoothSocket(device);
        } catch (Exception e) {
            Toast.makeText(getBaseContext(), "Socket creation failed", Toast.LENGTH_LONG).show();
        }
        // Establish the Bluetooth socket connection.
        try
        {
            btSocket.connect();
        } catch (Exception e) {
            try
            {
                btSocket.close();
            } catch (Exception e2)
            {
                //insert code to deal with this
            }
        }
        mConnectedThread = new ConnectedThread(btSocket);
        mConnectedThread.start();
        //I send a character when resuming.beginning transmission to check device is connected
        //If it is not an exception will be thrown in the write method and finish() will be called
        mConnectedThread.write("x");
    }

    @Override
    public void onPause()
    {
        super.onPause();
        try
        {
            //Don't leave Bluetooth sockets open when leaving activity
            btSocket.close();
        } catch (Exception e2) {
            //insert code to deal with this
        }
    }

    //Checks that the Android device Bluetooth is available and prompts to be turned on if off
    private void checkBTState() {

        if(btAdapter==null) {
            Toast.makeText(getBaseContext(), "Device does not support bluetooth", Toast.LENGTH_LONG).show();
        } else {
            if (btAdapter.isEnabled()) {
            } else {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, 1);
            }
        }
    }

    //create new class for connect thread
    private class ConnectedThread extends Thread {
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        //creation of the connect thread
        public ConnectedThread(BluetoothSocket socket) {
            InputStream tmpIn = null;
            OutputStream tmpOut = null;


            try {
                //Create I/O streams for connection
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (Exception e) { }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }


        public void run() {
            byte[] buffer = new byte[256];
            int bytes  = 0;
            byte[] file1Bytes = new byte[80000];

            // Keep looping to listen for received messages
            while (true) {
                try {
                    bytes = mmInStream.read(buffer);        	//read bytes from input buffer
                    String readMessage = new String(buffer, 0, bytes);
                    // Send the obtained bytes to the UI Activity via handler
                    bluetoothIn.obtainMessage(handlerState, bytes, -1, readMessage).sendToTarget();
                } catch (Exception e) {
                    break;
                }
            }
        }
        //write method
        public void write(String input) {
            byte[] msgBuffer = input.getBytes();           //converts entered String into bytes
            try {
                mmOutStream.write(msgBuffer);                //write bytes over BT connection via outstream
            } catch (Exception e) {
                //if you cannot write, close the application
                Toast.makeText(getBaseContext(), "Connection Failure", Toast.LENGTH_LONG).show();
                finish();

            }
        }
    }
}

