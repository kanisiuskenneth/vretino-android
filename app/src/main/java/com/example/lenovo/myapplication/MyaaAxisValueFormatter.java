package com.example.lenovo.myapplication;

/**
 * Created by lenovo on 5/14/2018.
 */
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.text.DecimalFormat;

public class MyaaAxisValueFormatter implements IAxisValueFormatter, com.example.lenovo.myapplication.MyAxisValueFormatter {
    /**
     * this is only needed if numbers are returned, else return 0
     */
    @Override
    public int getDecimalDigits() {
        return 1;
    }

    private DecimalFormat mFormat;

    public MyaaAxisValueFormatter() {

        // format values to 1 decimal digit
        mFormat = new DecimalFormat("###,###,##0.0");
    }

    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        // "value" represents the position of the label on the axis (x or y)
        return mFormat.format(((value + 1 - 256) * 1000 / 256)) + " ms";
    }
}
