package com.example.lenovo.myapplication;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import org.apache.commons.lang3.ArrayUtils;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;


public class ResultActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    private LineChart mChart;
    private InputStream inputStream;
    protected int lh;
    protected int lg;
    protected int lrh;
    protected int lrg;


    /**
     * The coefficients of the mother scaling (low pass filter) for decomposition.
     */
    protected double[ ] _scalingDeCom;

    /**
     * The coefficients of the mother wavelet (high pass filter) for
     * decomposition.
     */
    protected double[ ] _waveletDeCom;
    /**
     * The coefficients of the mother scaling (low pass filter) for
     * reconstruction.
     */
    protected double[ ] _scalingReCon;

    /**
     * The coefficients of the mother wavelet (high pass filter) for
     * reconstruction.
     */
    protected double[ ] _waveletReCon;


    @TargetApi(Build.VERSION_CODES.N)
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        mChart = (LineChart) findViewById(R.id.chart1);
        final TextView ltextview =(TextView) findViewById(R.id.textView2);
        final TextView ltextview2 =(TextView) findViewById(R.id.textView3);
        final Button btScreen = (Button) findViewById(R.id.button);
        mChart.setDragEnabled(true);
        mChart.setScaleEnabled(false);
        float xaxis ;
        final int sr = 256;
        final int samples = 512; //jumlah sampel

        btScreen.setOnClickListener(new View.OnClickListener() {
                                        public void onClick(View v) {
                                            takeScreenshot();
                                        }
                                    });

        XAxis xAxis = mChart.getXAxis();
        xAxis.setTextSize(11f);
        xAxis.setTextColor(Color.BLACK);
        xAxis.setDrawGridLines(false);
        xAxis.setDrawAxisLine(false);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setValueFormatter(new MyaaAxisValueFormatter());
        xAxis.setGranularity(1f);

        YAxis leftAxis = mChart.getAxisLeft();
        //leftAxis.setAxisMaximum(10);
        //leftAxis.setAxisMinimum(-10);
        leftAxis.setDrawGridLines(true);
        leftAxis.setGranularityEnabled(true);
        leftAxis.getAxisMaximum();
        leftAxis.getAxisMinimum();

        YAxis rightAxis = mChart.getAxisRight();
        // rightAxis.setAxisMaximum(10);
        //rightAxis.setAxisMinimum(-10);
        rightAxis.setDrawGridLines(true);
        rightAxis.setGranularityEnabled(true);
        leftAxis.getAxisMaximum();
        leftAxis.getAxisMinimum();

        ArrayList<Entry> last = new ArrayList<>();

        ArrayList<Double> tes = new ArrayList<>();


        File file = new File(getBaseContext().getExternalFilesDir(null), "apalahartiakumenunggu1.csv");
        FileInputStream fileInput = null;
        try {
            fileInput = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        //inputStream = getResources().openRawResource(R.raw.dataseteeg);
        BufferedReader reader = new BufferedReader(new InputStreamReader(fileInput));
        try {
            String csvLine;

            while ((csvLine = reader.readLine()) != null) {

                String[] datasteeg = csvLine.split(",");
                ;
                double data1 = Double.parseDouble(datasteeg[0]);

                tes.add(data1);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        /**inputStream = getResources().openRawResource(R.raw.isfan31);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        try {
            String csvLine;

            while ((csvLine = reader.readLine()) != null) {

                String[] datasteeg = csvLine.split(",");
                ;
                double data1 = Double.parseDouble(datasteeg[0]);

                tes.add(data1);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }**/


        double [] x;
        x = ArrayUtils.toPrimitive(tes.toArray(new Double[0] ), 0.0F);
        int w = 0;
        final int lx = x.length;
        for (Double f : tes) {
            x[w] = f;
            w++;
        }


        int stim = 256; // waktu ketika mulai menangkap stimulus

        int sc = 6; // level denoising
        double [][] ydn= new double[sc][lx];
        double [][] cden = new double[sc][lx];
        int sweeps = lx / samples; // jumlah trial
        lh = lg = lrh = lrg = 20; // wavelength of mother wavelet


        _scalingDeCom = new double[lh];
        _scalingDeCom[0] = -0.000679744372783699;
        _scalingDeCom[1] = 0.002039233118351097;
        _scalingDeCom[2] = 0.005060319219611981;
        _scalingDeCom[3] = -0.020618912641105536;
        _scalingDeCom[4] = -0.014112787930175846;
        _scalingDeCom[5] = 0.09913478249423216;
        _scalingDeCom[6] = 0.012300136269419315;
        _scalingDeCom[7] = -0.32019196836077857;
        _scalingDeCom[8] = 0.0020500227115698858;
        _scalingDeCom[9] = 0.9421257006782068;
        _scalingDeCom[10] = 0.9421257006782068;
        _scalingDeCom[11] = 0.0020500227115698858;
        _scalingDeCom[12] = -0.32019196836077857;
        _scalingDeCom[13] = 0.012300136269419315;
        _scalingDeCom[14] = 0.09913478249423216;
        _scalingDeCom[15] = -0.014112787930175846;
        _scalingDeCom[16] = -0.020618912641105536;
        _scalingDeCom[17] = 0.005060319219611981;
        _scalingDeCom[18] = 0.002039233118351097;
        _scalingDeCom[19] = -0.000679744372783699;

        _waveletDeCom = new double[lg];
        _waveletDeCom[0] = 0;
        _waveletDeCom[1] = 0;
        _waveletDeCom[2] = 0;
        _waveletDeCom[3] = 0;
        _waveletDeCom[4] = 0;
        _waveletDeCom[5] = 0;
        _waveletDeCom[6] = 0;
        _waveletDeCom[7] = 0;
        _waveletDeCom[8] = 0.1767766952966369;
        _waveletDeCom[9] = -0.5303300858899107;
        _waveletDeCom[10] = 0.5303300858899107;
        _waveletDeCom[11] = -0.1767766952966369;
        _waveletDeCom[12] = 0;
        _waveletDeCom[13] = 0;
        _waveletDeCom[14] = 0;
        _waveletDeCom[15] = 0;
        _waveletDeCom[16] = 0;
        _waveletDeCom[17] = 0;
        _waveletDeCom[18] = 0;
        _waveletDeCom[19] = 0;


        _scalingReCon = new double[lrh];
        _scalingReCon[0] = 0;
        _scalingReCon[1] = 0;
        _scalingReCon[2] = 0;
        _scalingReCon[3] = 0;
        _scalingReCon[4] = 0;
        _scalingReCon[5] = 0;
        _scalingReCon[6] = 0;
        _scalingReCon[7] = 0;
        _scalingReCon[8] = 0.1767766952966369;
        _scalingReCon[9] = 0.5303300858899107;
        _scalingReCon[10] = 0.5303300858899107;
        _scalingReCon[11] = 0.1767766952966369;
        _scalingReCon[12] = 0;
        _scalingReCon[13] = 0;
        _scalingReCon[14] = 0;
        _scalingReCon[15] = 0;
        _scalingReCon[16] = 0;
        _scalingReCon[17] = 0;
        _scalingReCon[18] = 0;
        _scalingReCon[19] = 0;

        _waveletReCon = new double[lrg];
        _waveletReCon[0] = 0.000679744372783699;
        _waveletReCon[1] = 0.002039233118351097;
        _waveletReCon[2] = -0.005060319219611981;
        _waveletReCon[3] = -0.020618912641105536;
        _waveletReCon[4] = 0.014112787930175846;
        _waveletReCon[5] = 0.09913478249423216;
        _waveletReCon[6] = -0.012300136269419315;
        _waveletReCon[7] = -0.32019196836077857;
        _waveletReCon[8] = -0.0020500227115698858;
        _waveletReCon[9] = 0.9421257006782068;
        _waveletReCon[10] = -0.9421257006782068;
        _waveletReCon[11] = 0.0020500227115698858;
        _waveletReCon[12] = 0.32019196836077857;
        _waveletReCon[13] = 0.012300136269419315;
        _waveletReCon[14] = -0.09913478249423216;
        _waveletReCon[15] = -0.014112787930175846;
        _waveletReCon[16] = 0.020618912641105536;
        _waveletReCon[17] = 0.005060319219611981;
        _waveletReCon[18] = -0.002039233118351097;
        _waveletReCon[19] = -0.000679744372783699;


        // koefisien diinvert karena konvolusi
        int [][] den_coeff;
        den_coeff = new int[11][2];
        // koefisien untuk proses denoising
        for (int i = 0; i < 11; i++) {
            for (int j= 0 ; j <2; j++){
                if(j == 0) {
                    den_coeff[i][j] = 0;
                } else {
                    if(i >= sc) {
                        den_coeff[i][j] = 0;
                    } else {
                        Double penyebut = new Double(Math.pow(2,(i+1)));
                        den_coeff[i][j] = samples/penyebut.intValue();
                    }
                }
            }
        }
        double [] th = new double[lx];
        for (int i = 0; i < lx; i++) {
            th[i] = x[i];
            //  System.out.println("th : [" +i+ " ] " +th[i]);
        }

        int lth = th.length;
        double[] yden;
        yden = new double[lx];
        for (int s =0 ; s < sc; s++) {


            // inisiasi matriks untuk proses dekomposisi
            double [] tga ; // highpass filter
            double [] tha ; // lowpass filter
            // inisiasi matriks untuk
            double [] tgu = new double[lx];
            double [] tm = new double[lx];
            double[] tma = new double[lx];
            double [] tmau = new double[lx];
            double [] tman = new double[lx];
            double [] tmao = new double[lx];
            double [] tmap = new double[lx];
            double [] tmaq = new double[lx];

            // ini siasi matriks untuk proses
            double [] tg = new double[lx];
            double [] tmu = new double[lx];
            double [] tmv = new double[lx];
            double [] tmw = new double[lx];
            double [] tmx = new double[lx];
            double [] tmd = new double[lx];
            double [] tmdu = new double[lx];
            double [] tmda = new double[lx];
            double [] tmdb = new double[lx];
            double [] tmdc = new double [lx];
            double [] tmn = new double[lx];
            // inisiasi variabel panjang matriks
            int ltmau = tmau.length;
            int ltman = tmau.length;
            int ltmao = tmau.length;
            int ltmap = tmau.length;
            int ltmaq = tmau.length;
            int ltmn = tmn.length;
            tga = new double[lth];
            tha = new double[lth];
            int ltga = tga.length;
            int ltha = tha.length;
            int ltm = tm.length;
            int ltgu = tgu.length;
            int ltma = tma.length;
            // proses dekomposisi highpass filter
            for (int k = 0; k < lth; k++) {
                for (int a = -10; a < 10; a++) {
                    if ( k < 7){
                        if (a > -k) {
                            if (a < (lth - k)) {
                                tha[k] = tha[k] + (th[a + k-1] * _scalingDeCom[a + 10-1]);
                            }
                        }
                    }else {
                        if (a > -k) {
                            if (a < (lth - k)) {
                                tha[k] = tha[k] + (th[a + k] * _scalingDeCom[a + 10]);
                            }
                        }
                    }
                }
                // proses dekomposisi lowpass filter
                for (int a = -10; a < 10; a++) {
                    if(k< 3) {
                        if (a > -k) {
                            if (a < (lth - k)) {
                                tga[k] = tga[k] + (th[a + k-1] * _waveletDeCom[a + 10-1]);
                            }
                        }
                    } else {
                        if (a > -k) {
                            if (a < (lth - k)) {
                                tga[k] = tga[k] + (th[a + k] * _waveletDeCom[a + 10]);
                            }
                        }
                    }
                }
            }
            lth = (int) (lth / 2);
            int ltg = lth;
            for (int i = 0; i < ltg; i++) {

                tg[i] = tga[2*i];
            }
            for (int j = 0; j < lth; j++) {
                th[j] = tha[2*j];
            }
            // denoising detail level

            for (int sw = 0; sw < sweeps; sw++) {
                for (int j = ((sw * ltg )/ sweeps) ; j < ((sw * ltg)/sweeps + den_coeff[5][1]); j++) {
                    tg[j] = 0;
                }
            }
            for (int a = s ; a > -1; a--) {
                if (a == s) {
                    ltm = 2*ltg;
                    for (int j = 0; j < ltg; j++) {
                        tm[2*j] = tg[j];
                    }
                    ltma = ltm;
                    for (int u = 0; u < ltma; u++) {
                        for (int i = -10+1; i < 10+1; i++) {
                            if (i > -u) {
                                if (i < (ltma - u)) {
                                    tma[u] = tma[u] + tm[i + u] * _waveletReCon[i + 10-1];
                                }
                            }
                        }
                    }
                    for (int i = 0; i < ltma; i++) {
                        tmn[i] = tma[i];
                    }
                } else {
                    int ltmu = (int) Math.pow(2,s-a-1)*ltma;
                    if (a == s-1){
                        ltmu = 2* ltma;
                        for (int n = 0 ; n< ltmu/2  ; n++){
                            tmu[2*n] = tma[n];
                        }
                        for (int z = 0; z < ltmu; z++) {
                            for (int i = -10+1; i < 10+1; i++) {
                                if (i > -z) {
                                    if (i < (ltmu - z)) {
                                        tmau[z] = tmau[z] + tmu[i + z] * _scalingReCon[i + 10-1];
                                    }
                                }
                            }
                        }
                        for (int i = 0; i < ltmau; i++) {
                            tmd[i] = tmau[i];
                        }
                    }
                    if ((a == s-2)) {
                        ltmu = 2* ltmu;
                        for (int n = 0 ; n< ltmu/2  ; n++){
                            tmu[2*n] = tmd[n];
                        }
                        ltman =ltmu;
                        for (int z = 0; z < ltman; z++) {
                            for (int i = -10+1; i < 10+1; i++) {
                                if (i > -z) {
                                    if (i < (ltman - z)) {
                                        tman[z] = tman[z] + tmu[i + z] * _scalingReCon[i + 10-1];
                                    }
                                }
                            }
                        }
                        for (int i = 0; i < ltmau; i++) {
                            tmdu[i] = tman[i];
                        }
                    }
                    if ((a == s-3)){
                        int ltmv = tmv.length;
                        ltmv = 2* ltmu;
                        for (int n = 0 ; n< ltmv/2  ; n++){
                            tmv[2*n] = tmdu[n];
                        }
                        ltmao =ltmv;
                        for (int z = 0; z < ltmv; z++) {
                            for (int i = -10+1; i < 10+1; i++) {
                                if (i > -z) {
                                    if (i < (ltmv - z)) {
                                        tmao[z] = tmao[z] + tmv[i + z] * _scalingReCon[i + 10-1];
                                    }
                                }
                            }
                        }
                        for (int i = 0; i < ltmau; i++) {
                            tmda[i] = tmao[i];
                        }
                    }
                    if ((a == s-4)){
                        int ltmw = tmw.length;
                        ltmw = 2* ltmu;
                        for (int n = 0 ; n< ltmw/2  ; n++){
                            tmw[2*n] = tmda[n];
                        }
                        ltmap =ltmw;
                        for (int z = 0; z < ltmw; z++) {
                            for (int i = -10+1; i < 10+1; i++) {
                                if (i > -z) {
                                    if (i < (ltmw - z)) {
                                        tmap[z] = tmap[z] + tmw[i + z] * _scalingReCon[i + 10-1];
                                    }
                                }
                            }
                        }
                        for (int i = 0; i < ltmau; i++) {
                            tmdb[i] = tmap[i];
                        }
                    }
                    if ((a == s-5)){
                        int ltmx;
                        ltmx = 2* ltmu;
                        for (int n = 0 ; n< ltmx/2  ; n++){
                            tmx[2*n] = tmdb[n];
                        }
                        ltmaq =ltmx;
                        for (
                                int z = 0; z < ltmx; z++) {
                            for (int i = -10+1; i < 10+1; i++) {
                                if (i > -z) {
                                    if (i < (ltmx - z)) {
                                        tmaq[z] = tmaq[z] + tmx[i + z] * _scalingReCon[i + 10-1];
                                    }
                                }
                            }
                        }
                        for (int i = 0; i < ltmau; i++) {
                            tmdc[i] = tmaq[i];
                        }
                    }
                }

            }
            for (int j = 0; j < lx; j++) {
                if (s==0){
                    ydn[s][j] = tmn[j];
                } else if (s==1){
                    ydn[s][j] = tmd[j];
                }else if (s==2) {
                    ydn[s][j] = tmdu[j];
                }else if (s==3){
                    ydn[s][j] = tmda[j];
                } else if (s==4){
                    ydn[s][j] = tmdb[j];
                } else if (s==5){
                    ydn[s][j] = tmdc[j];
                }
                // yden[j] = ydn[0][j]+ ydn[1][j] + ydn[2][j] + ydn[3][j]+ ydn[4][j]+ ydn[5][j];
            }
        }
// reshape and make average
        double[][] dd;
        dd = new double[sweeps][samples];
        double ydenav[];
        ydenav = new double[samples];

        for (int g = 0 ; g < sweeps ; g++){
            for(int q = 0 ; q < samples ; q++){
                dd[g][q] = ydn[4][(g*samples)+q];
            }
        }
        double [] yuwi = new double[samples];
        for (int  i = 0; i < samples; i++) {
            for (int j = 0 ; j < sweeps ; j++){
                yuwi[i] += dd[j][i];
            }
            ydenav[i] = yuwi[i]/sweeps;
        }
        for (int d = stim-51; d < samples-128 ; d++ ){
            last.add(new Entry(d  , (float) ydenav[d]));
        }

        double largest = ydenav[0];
        int index = 0;
        for (int i = 0; i < samples-128; i++) {
            if ( ydenav[i] > largest ) {
                largest = ydenav[i];
                index = i;
            }
        }
        ltextview.setText("Amplitude P100 : " +largest + "uV");
        ltextview2.setText("latency P100 : " +((index+1-stim)*1000/sr) + "ms");
        LineDataSet dataSet;
        dataSet = new LineDataSet(last, "EEGData");
        dataSet.setValues(last);
        dataSet.setFillAlpha(110);
        dataSet.setLineWidth(1f);
        dataSet.setDrawCircles(false);
        dataSet.setDrawValues(false);
        dataSet.setDrawCircleHole(false);
        dataSet.setColor(Color.BLUE);
        ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(dataSet);
        LineData datas = new LineData(dataSets);
        mChart.setData(datas);
    }

    private void takeScreenshot() {
        Date now = new Date();
        android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);

        try {
            // image naming and path  to include sd card  appending name you choose for file

            File directory = getExternalFilesDir(null);
            // create bitmap screen capture
            View v1 = getWindow().getDecorView().getRootView();
            v1.setDrawingCacheEnabled(true);
            Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
            v1.setDrawingCacheEnabled(false);
            File imageFile = new File(directory,"Data "+" "+ now + ".png");
            FileOutputStream outputStream = new FileOutputStream(imageFile);
            int quality = 100;
            bitmap.compress(Bitmap.CompressFormat.PNG, quality, outputStream);
            outputStream.flush();
            outputStream.close();


        } catch (Throwable e) {
            // Several error may come out with file handling or OOM
            e.printStackTrace();
        }
    }
}

