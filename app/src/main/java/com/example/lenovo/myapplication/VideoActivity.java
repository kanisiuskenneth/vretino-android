package com.example.lenovo.myapplication;

/**
 * Created by lenovo on 5/3/2018.
 */
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.VideoView;

public class VideoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_video);
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        // getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        VideoView videoView = (VideoView) findViewById(R.id.myvideoview);
        String uriPath = "android.resource://" + getPackageName() + "/" + R.raw.stimvid;
        Uri uri = Uri.parse(uriPath);

        videoView.setVideoURI(uri);
        videoView.start();

    }
}