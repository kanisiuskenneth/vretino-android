package com.example.lenovo.myapplication;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.p2p.WifiP2pManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button btButton = (Button) findViewById(R.id.buttonsabun);
        ImageButton infoButton = (ImageButton) findViewById(R.id.imageButton);

        btButton.setOnClickListener(new View.OnClickListener() {
                                        public void onClick(View view) {
                                            //saat diklik pindah ke RecordActivity
                                            Intent intent = new Intent(MainActivity.this, DeviceListActivity.class);
                                            startActivity(intent);
                                            //pada Record Activity mulai koneksi bluetooth
                                        }
                                    });

        infoButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                //saat diklik pindah ke RecordActivity
                Intent intent = new Intent(MainActivity.this, InfoActivity.class);
                startActivity(intent);
                //pada Record Activity mulai koneksi bluetooth
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();

    }
}


